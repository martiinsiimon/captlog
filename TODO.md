GUI
* zoom-in/out to the actual center
* export popover with all options to export to
* add settings menu option
* right-click action to remove particular visit


Export
* export to png/jpg
* export text information


Settings
* set color of background/visited/unvisited
* set zoom boundaries
* reset visited file


Other
* add binary releases to git
* proper linux installation
