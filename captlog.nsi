!define CAPTLOG_VERSION XXXX_VERSION_XXXX
!define PRODUCT_VERSION "${CAPTLOG_VERSION}-win64"
!define PRODUCT_NAME "CaptLog"
!define PRODUCT_NAME_LONG "Kapitansky denik"
!define PRODUCT_PUBLISHER "Martin Simon"
!define PRODUCT_WEB_SITE "https://gitlab.com/martiinsiimon/captlog"
!define INSTALLER_OUTPUT_FILE "captlog-${PRODUCT_VERSION}.exe"

!define BUILD_DIR "build_win64"


!define PRODUCT_DIR_REGKEY "Software\Microsoft\Windows\CurrentVersion\App Paths\AppMainExe.exe"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"

!define REGISTRY_APP_PATHS "Software\Microsoft\Windows\CurrentVersion\App Paths"


; AddToPath and friends should work with all users
!define ALL_USERS


; --------------- General Settings
; this is needed for proper start menu item manipulation (for all users) in vista
RequestExecutionLevel admin

; This compressor gives us the best results
SetCompressor /SOLID lzma

; Do a CRC check before installing
CRCCheck On


; This is used in titles
Name "${PRODUCT_NAME_LONG}"  ;  ${PRODUCT_VERSION}

; Output File Name
OutFile "${INSTALLER_OUTPUT_FILE}"


; The Default Installation Directory
InstallDir "$PROGRAMFILES64\${PRODUCT_NAME}"
;InstallDir "$WINDIR"
; Detect the old installation
InstallDirRegKey HKLM "SOFTWARE\${PRODUCT_NAME}" ""
;InstallDirRegKey HKLM "${PRODUCT_DIR_REGKEY}" ""

ShowInstDetails "nevershow"
ShowUninstDetails "nevershow"
; ShowInstDetails show
; ShowUninstDetails show

!include "MUI2.nsh"


;---------------------------------
;General

  !define MUI_ICON "data/compass.ico"
  !define MUI_UNICON "data/compass.ico"
  !define MUI_SPECIALBITMAP "data/compass.png"

;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_LICENSE "LICENSE"
  ;!insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_INSTFILES

  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES


;--------------------------------
;Languages

  !insertmacro MUI_LANGUAGE "English"
  !insertmacro MUI_LANGUAGE "Czech"

;--------------------------------
;Installer Sections
;Section "Install" Installation info
Section "Install" Installation

;Add files
  SetOutPath "$INSTDIR\bin"
  File "${BUILD_DIR}/src/captlog.exe"

  SetOutPath "$INSTDIR\data"
  File "data/map.dat"
  File "data/compass.ico"
  File "data/compass.png"

;create desktop shortcut
  CreateShortCut "$DESKTOP\${PRODUCT_NAME_LONG}.lnk" "$INSTDIR\bin\captlog.exe" "" "$INSTDIR\data\compass.ico" 0

;create start-menu items
  CreateDirectory "$SMPROGRAMS\${PRODUCT_NAME_LONG}"
  CreateShortCut "$SMPROGRAMS\${PRODUCT_NAME_LONG}\Uninstall.lnk" "$INSTDIR\Uninstall.exe" "" "$INSTDIR\data\compass.ico" 0
  CreateShortCut "$SMPROGRAMS\${PRODUCT_NAME_LONG}\${PRODUCT_NAME_LONG}.lnk" "$INSTDIR\bin\captlog.exe" "" "$INSTDIR\data\compass.ico" 0

;write uninstall information to the registry
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}" "DisplayName" "${PRODUCT_NAME} (remove only)"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}" "UninstallString" "$INSTDIR\Uninstall.exe"

  WriteUninstaller "$INSTDIR\Uninstall.exe"

SectionEnd

;--------------------------------
;Uninstaller Section
Section "Uninstall"

;Delete Files
  RMDir /r "$INSTDIR\*.*"

;Remove the installation directory
  RMDir "$INSTDIR"

;Delete Start Menu Shortcuts
  Delete "$DESKTOP\${PRODUCT_NAME_LONG}.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME_LONG}\*.*"
  RmDir  "$SMPROGRAMS\${PRODUCT_NAME_LONG}"

;Delete Uninstaller And Unistall Registry Entries
  DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\${PRODUCT_NAME}"
  DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"

SectionEnd


;eof
