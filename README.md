# Captain's Log #
Imagine you are a captain of an ocean liner and you are curious what countries you have visited so far. Then this simple application is exactly for you.

## Author ##
Martin Simon

## Descriptions ##
Although what is written above, this simple GTKmm-based multi-platform application was developed as a private and purely academic project - to learn basics of [CMake](https://cmake.org/), [GTK+](http://www.gtk.org/), [GTKmm](http://www.gtkmm.org), Win32 installer (e.g. [NSIS](http://nsis.sourceforge.net)) and to update knowledge of C++ in respect of C++14 standard.

## Building & Installation ##
To install captlog, one can use pre-compiled binaries or to build from source codes. If you are not familiar with building from source codes, I recommend to use those binaries located in [releases directory](releases). Otherwise, take a look at the following steps to build it on your own.

### Getting source codes ###
The primary online source of this project is this location, I mean [GitLab repository](https://gitlab.com/martiinsiimon/captlog). You can clone the repository with following commands:

```
$ git clone https://gitlab.com/martiinsiimon/captlog.git
```

**Requirements:** *git*


### Building on Linux ###
Go to the directory `captlog` and execute following steps:

```
$ mkdir build_linux
$ cd build_linux
$ cmake -DCMAKE_BUILD_TYPE=Release ..
$ make
```

**Requirements:** *cmake*, *make*, *g++*, *gtkmm-devel*


### Creating Linux (Debian) package ###
Go to the directory `captlog/scripts` and execute following steps:

```
$ bash buildDebianInstaller.sh
```

Then you can find your debian package in `captlog/releases`.

**Requirements:** *dpkg-deb*, *built Linux binaries (step above)*


### Installing locally on Linux ###
In `captlog/releases` directory execute following steps:

```
$ sudo dpkg -i captlog.deb
```

**Note**: You need to substitute the package name with the actual name (use `ls captlog/releases` to see all files).

**Requirements**: *built Linux package (step above)*, *satisfied dependencies*


### Building for Windows (on Linux) ###
Go to the directory `captlog` and execute following steps:

```
$ mkdir build_win64
$ cd build_win64
$ cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=../Toolchain-mingw64.cmake ..
$ make
```

**Requirements:** *cmake*, *make*, *x86_64-w64-mingw32-g++*, *gtkmm-devel*


### Creating Windows installer ###
Go to the directory `captlog/scripts` and execute following steps:

```
$ bash buildWindowsInstaller.sh
```

Then you can find your installation binary in `captlog/releases`.

**Requirements:** *nsis*, *built Windows binaries (step above)*


### Installing locally on Windows ###
In `captlog/releases` directory execute the binary installer (from the step above).

**Requirements:** *gtk runtime libraries installed*


## Usage ##
CaptLog is a GUI application, which can be executed by command `captlog` after successful installation. The usage should be as simply as possible and I suppose it does not require any further instructions.
