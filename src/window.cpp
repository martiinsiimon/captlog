#include "window.h"
#include "map.h"

#include <gdkmm/pixbuf.h>
#include <iostream>
#include <fstream>

#include <ctime>

#ifdef WIN32
#define _(x) x
#else
#include <glibmm/i18n.h>
#endif

CL::MainWindow::MainWindow(CL::Map &map) : m_map(map)
{
    bool ask_for_name = !this->load_config();

    if (ask_for_name)
    {
        this->show_ask_for_name_dialog();
    }

    this->set_default_size(1000, 800);

    this->set_title("Kapitánský deník");

    this->m_button_add.set_halign(Gtk::ALIGN_START);
    this->m_button_add.set_valign(Gtk::ALIGN_START);
    this->m_button_add.set_label(_("Přidat"));

    this->m_button_export.set_halign(Gtk::ALIGN_START);
    this->m_button_export.set_valign(Gtk::ALIGN_START);
    this->m_button_export.set_label(_("Tisk"));

    this->m_button_quit.set_halign(Gtk::ALIGN_END);
    this->m_button_quit.set_valign(Gtk::ALIGN_START);
    this->m_button_quit.set_label(_("Ukončit"));

    this->m_button_options.set_halign(Gtk::ALIGN_END);
    this->m_button_options.set_valign(Gtk::ALIGN_START);
    this->m_button_options.set_image_from_icon_name("gnome-settings");

    this->m_headerbar_left_box.pack_start(this->m_button_add, Gtk::PACK_SHRINK);
    this->m_headerbar_left_box.pack_start(this->m_button_export, Gtk::PACK_SHRINK);
    this->m_headerbar_left_box.set_spacing(10);

    this->m_headerbar.pack_start(this->m_headerbar_left_box);

    this->m_headerbar.set_title(_("Kapitánský deník"));
    this->m_headerbar.set_subtitle(this->m_captains_name);
    this->m_headerbar.set_show_close_button(false);

    this->m_headerbar_right_box.pack_start(this->m_button_options, Gtk::PACK_SHRINK);
    this->m_headerbar_right_box.pack_start(this->m_button_quit, Gtk::PACK_SHRINK);
    this->m_headerbar_right_box.set_spacing(10);
    this->m_headerbar.pack_end(this->m_headerbar_right_box);

    this->m_frame.add(map);
    this->add(this->m_frame);
    this->set_titlebar(this->m_headerbar);

    // Connect the signal to hide the window:
    this->m_button_add.signal_clicked().connect(sigc::mem_fun(*this, &CL::MainWindow::on_button_add_clicked));
    this->m_button_export.signal_clicked().connect(sigc::mem_fun(*this, &CL::MainWindow::on_button_export_clicked));
    this->m_button_options.signal_clicked().connect(sigc::mem_fun(*this, &CL::MainWindow::show_ask_for_name_dialog));
    this->m_button_quit.signal_clicked().connect(sigc::mem_fun(*this, &CL::MainWindow::on_button_quit_clicked));

    this->m_dialog_add.set_title(_("Přidat turistu ze země"));
    this->m_dialog_add.set_transient_for(*this);
    this->m_dialog_add.set_modal(true);

    this->m_dialog_button_add.set_label(_("Přidat"));

    this->m_dialog_button_add.signal_clicked().connect(sigc::mem_fun(*this, &CL::MainWindow::on_dialog_button_add_click));
    this->m_dialog_search_entry.signal_activate().connect(sigc::mem_fun(*this, &CL::MainWindow::on_dialog_button_add_click));

    this->m_dialog_add.get_content_area()->pack_start(this->m_dialog_search_entry);
    this->m_dialog_add.get_content_area()->pack_start(this->m_dialog_button_add);

    //Add an EntryCompletion:
    auto completion = Gtk::EntryCompletion::create();
    this->m_dialog_search_entry.set_completion(completion);

    //Create and fill the completion's filter model
    auto refCompletionModel = Gtk::ListStore::create(this->m_Columns);
    completion->set_model(refCompletionModel);

    //Fill the TreeView's model
    Gtk::TreeModel::Row row;

    for (unsigned int i = 0; i < this->m_map.countries.size(); i++)
    {
        row = *(refCompletionModel->append());
        row[this->m_Columns.m_col_id] = i;
        row[this->m_Columns.m_col_name] = this->m_map.countries[i].name;
    }

    completion->set_text_column(this->m_Columns.m_col_name);

    //popover settins
    this->m_popover_export.set_relative_to(this->m_button_export);

    this->m_export_image.set_label(_("Obrázek"));
    this->m_export_image.signal_clicked().connect(sigc::mem_fun(*this, &CL::MainWindow::on_export_image_click));

    this->m_export_vbox.pack_start(this->m_export_image);
    this->m_export_vbox.show_all();

    this->m_popover_export.add(this->m_export_vbox);
    this->m_popover_export.set_position(Gtk::POS_BOTTOM);
    this->m_popover_export.set_border_width(6);

    // as for name if needed
    this->show_all_children();
}

CL::MainWindow::~MainWindow()
{
}

void CL::MainWindow::on_button_add_clicked()
{
    // Clear the entry field
    this->m_dialog_search_entry.set_text("");

    // Grab focus for the entry
    this->m_dialog_search_entry.grab_focus();

    // Grab default for the button
    this->m_dialog_button_add.set_can_default();
    this->m_dialog_button_add.grab_default();

    this->m_dialog_add.show_all();
}

void CL::MainWindow::on_dialog_name_click()
{
    std::string name = this->m_dialog_name_entry.get_text();
    if (name != "")
    {
        this->m_captains_name = name;
        this->m_headerbar.set_subtitle(this->m_captains_name);

        // store captain's name to a config file
        // TODO move to the config class
        std::string path;
#ifdef WIN32
        path = std::string(getenv("APPDATA")) + "\\CaptLog\\";
#else
        path = Glib::get_home_dir() + "/.captlog/";
#endif
        std::ofstream fon(path + "config.cfg");
        fon << "name#" << this->m_captains_name << std::endl;

        this->m_dialog_name.hide();
        this->show_all_children();
    }
}

void CL::MainWindow::on_dialog_button_add_click()
{
    if (this->m_map.add_visit(this->m_dialog_search_entry.get_text()))
    {
        this->m_map.queue_draw();
        this->m_map.store_visits();
    }

    this->m_dialog_add.hide();
}

void CL::MainWindow::on_button_export_clicked()
{
    this->m_popover_export.set_visible(true);
}

void CL::MainWindow::on_export_image_click()
{
    // Create and show export dialog
    Gtk::FileChooserDialog dialog(*this, "Export", Gtk::FILE_CHOOSER_ACTION_SAVE);

    // Add response buttons to the dialog:
    dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
    dialog.add_button("Vybrat", Gtk::RESPONSE_OK);

    // Pre-fill the file name column
    char buffstr[100];
    const time_t act_time = time(NULL);
    strftime(buffstr, 99, "%Y-%m-%d-", localtime(&act_time));
    dialog.set_current_name(std::string(buffstr) + this->m_captains_name + "-CaptLog.png");

    // TODO set folder to home (for both linux and win)
    std::string path;
#ifdef WIN32
    path = std::string(getenv("HOMEDIR"));
#else
    path = Glib::get_home_dir();
#endif
    dialog.set_current_folder(path);

    int result = dialog.run();
    if (result == Gtk::RESPONSE_OK)
    {
        std::string new_file_name = dialog.get_current_name();
        std::string new_folder = dialog.get_current_folder();

        // TODO check if the extension is png and if not, append it
        //if (new_file_name.substr())
        //{
        //    new_file_name += ".png";
        //}

        this->m_map.store_drawing_to(new_folder + "/" + new_file_name);
    }
    this->m_popover_export.set_visible(false);
}

void CL::MainWindow::on_button_quit_clicked()
{
    hide();
}

/**
 * @todo move to Config class
 */
bool CL::MainWindow::load_config()
{
    this->m_captains_name = _("Jack Sparrow");

    std::string path;
#ifdef WIN32
    path = std::string(getenv("APPDATA")) + "\\CaptLog\\";
#else
    path = Glib::get_home_dir() + "/.captlog/";
#endif

    struct stat sb;

    if (stat(path.c_str(), &sb) != 0 || !S_ISDIR(sb.st_mode))
    {
        MKDIR(path.c_str());
        return false;
    }

    std::ifstream fin(path + "config.cfg");

    if (!fin.fail())
    {
        std::ifstream config_file(path + "config.cfg");

        std::string buffer;
        while (std::getline(config_file, buffer))
        {
            //FORMAT: name#N
            size_t pos = 0;
            if ((pos = buffer.find('#')) != std::string::npos && buffer.substr(0, pos) == "name")
            {
                this->m_captains_name = buffer.substr(pos + 1, buffer.length() - pos);
                return true;
            }
        }
    }
    return false;
}

void CL::MainWindow::show_ask_for_name_dialog()
{
    this->m_dialog_name.set_title(_("Jméno kapitána"));
    this->m_dialog_name.set_transient_for(*this);
    this->m_dialog_name.set_modal(true);

    this->m_dialog_name_button.set_label(_("Nastavit"));

    this->m_dialog_name_button.signal_clicked().connect(sigc::mem_fun(*this, &CL::MainWindow::on_dialog_name_click));
    this->m_dialog_name_entry.signal_activate().connect(sigc::mem_fun(*this, &CL::MainWindow::on_dialog_name_click));

    this->m_dialog_name.get_content_area()->pack_start(this->m_dialog_name_entry);
    this->m_dialog_name.get_content_area()->pack_start(this->m_dialog_name_button);

    this->m_dialog_name.show_all();
    this->m_dialog_name.run();
}