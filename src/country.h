
#pragma once

#include "coordinate.h"

#include <string>
#include <vector>

namespace CL
{

class Country
{
public:
  Country();
  Country(std::string name, bool visited, int visit_nr);
  virtual ~Country();

  void increment_visits();
  void decrement_visits();

  std::string name;
  bool visited;
  int visits;

  std::vector<std::vector<Coordinate>> coordinates;
};
} // namespace CL
