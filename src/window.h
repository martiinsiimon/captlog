#pragma once

#include "map.h"

#include <gtkmm.h>

namespace CL
{
class MainWindow : public Gtk::Window
{
  public:
    MainWindow(CL::Map &da);
    virtual ~MainWindow();

  protected:
    //Signal handlers:
    void on_button_add_clicked();
    void on_button_export_clicked();
    void on_button_quit_clicked();
    void on_dialog_button_add_click();
    void on_dialog_name_click();
    void on_export_image_click();

    //Tree model columns, for the EntryCompletion's filter model:

    class ModelColumns : public Gtk::TreeModel::ColumnRecord
    {
      public:
        ModelColumns()
        {
            add(m_col_id);
            add(m_col_name);
        }

        Gtk::TreeModelColumn<unsigned int> m_col_id;
        Gtk::TreeModelColumn<Glib::ustring> m_col_name;
    };

    ModelColumns m_Columns;

    typedef std::map<int, Glib::ustring> type_actions_map;
    type_actions_map m_CompletionActions;

  private:
    std::string m_captains_name;

    //Child widgets:
    Gtk::Button m_button_add;
    Gtk::Button m_button_export;
    Gtk::Button m_button_quit;
    Gtk::Button m_button_options;

    Gtk::HBox m_headerbar_left_box;
    Gtk::HBox m_headerbar_right_box;
    Gtk::HeaderBar m_headerbar;

    Gtk::Frame m_frame;

    CL::Map &m_map;

    Gtk::Dialog m_dialog_add;
    Gtk::Button m_dialog_button_add;
    Gtk::Entry m_dialog_search_entry;

    Gtk::Popover m_popover_export;
    Gtk::VBox m_export_vbox;
    Gtk::Button m_export_image;

    Gtk::Dialog m_dialog_name;
    Gtk::Button m_dialog_name_button;
    Gtk::Entry m_dialog_name_entry;

    bool load_config();

    void show_ask_for_name_dialog();
};
} // namespace CL
