#pragma once

namespace CL
{

class Coordinate
{
  public:
    Coordinate() : x(0), y(0){};
    Coordinate(float _x, float _y);
    virtual ~Coordinate();
    float x;
    float y;
};
} // namespace CL
