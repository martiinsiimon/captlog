#include "map.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <glibmm-2.4/glibmm/miscutils.h>

CL::Map::Map()
{
    this->scale = 1.0f;
    this->translate_x = 0.0f;
    this->translate_y = 0.0f;

    this->translate_x_pers = 0.0f;
    this->translate_y_pers = 0.0f;

    this->high_x = 0.0f;
    this->high_y = 0.0f;
    this->low_x = 0.0f;
    this->low_y = 0.0f;

    this->button_pressed = false;
    this->store = false;

    this->add_events(Gdk::SCROLL_MASK | Gdk::BUTTON_PRESS_MASK | Gdk::BUTTON_RELEASE_MASK | Gdk::BUTTON_MOTION_MASK);

    this->signal_scroll_event().connect(sigc::mem_fun(*this, &CL::Map::on_scroll_event));
    this->signal_button_press_event().connect(sigc::mem_fun(*this, &CL::Map::on_button_press_event));
    this->signal_button_release_event().connect(sigc::mem_fun(*this, &CL::Map::on_button_press_event));
    this->signal_motion_notify_event().connect(sigc::mem_fun(*this, &CL::Map::on_motion_notify_event));

#ifdef WIN32
    this->load_map("../data/map.dat");
#else
    this->load_map("/usr/local/share/captlog/data/map.dat");
#endif
}

bool CL::Map::on_draw(const Cairo::RefPtr<Cairo::Context> &cr)
{
    Gtk::Allocation allocation = this->get_allocation();
    const float width = static_cast<float>(allocation.get_width());
    const float height = static_cast<float>(allocation.get_height());

    // coordinates for the center of the window
    float xc = width / 2.0f + this->translate_x;
    float yc = height / 2.0f + this->translate_y;

    float as_x = this->base_width / (this->high_x - this->low_x) * this->scale;
    float as_y = this->base_height / (this->high_y - this->low_y) * this->scale * -1.0f;

    cr->set_source_rgb(0.9, 0.9, 0.9);

    cr->paint();

    cr->set_line_width(1.0);

    for (CL::Country const &country : this->countries)
    {
        for (std::vector<CL::Coordinate> const &polygon : country.coordinates)
        {
            if (polygon.size() == 0)
                continue;

            cr->move_to(polygon[0].x * as_x + xc, polygon[0].y * as_y + yc);

            for (CL::Coordinate const &coordinate : polygon)
            {
                cr->line_to(coordinate.x * as_x + xc, coordinate.y * as_y + yc);
            }
        }

        if (country.visited)
            cr->set_source_rgb(0.2, 0.8, 0.2);
        else
            cr->set_source_rgb(0.2, 0.2, 0.2);

        cr->fill();
    }

    if (this->store)
    {
        auto pixbuf = Gdk::Pixbuf::create(cr->get_target(), allocation.get_x(), allocation.get_y(), width, height);

        pixbuf->save(this->store_filename, "png");

        this->store = false;
    }
    return true;
}

bool CL::Map::on_scroll_event(GdkEventScroll *e)
{
    //std::cout << "scrollEvent on [" << e->x << "," << e->y << "]" << std::endl;
    //TODO add coordinates to center image
    bool redraw = false;
    if (e->direction == GDK_SCROLL_DOWN && this->scale > this->min_scale)
    {
        this->scale *= 0.95f;
        redraw = true;
    }
    else if (e->direction == GDK_SCROLL_UP && this->scale < this->max_scale)
    {
        this->scale *= 1.05f;
        redraw = true;
    }

    if (redraw)
        this->queue_draw();

    return true;
}

bool CL::Map::on_button_press_event(GdkEventButton *e)
{
    if (e->button == GDK_BUTTON_PRIMARY && e->type == GDK_BUTTON_PRESS)
    {
        this->press_x = e->x;
        this->press_y = e->y;
        this->button_pressed = true;
    }
    else if (e->button == GDK_BUTTON_PRIMARY && e->type == GDK_BUTTON_RELEASE)
    {
        this->button_pressed = false;
        this->translate_x_pers = this->translate_x;
        this->translate_y_pers = this->translate_y;
    }

    return true;
}

bool CL::Map::on_motion_notify_event(GdkEventMotion *e)
{
    if (this->button_pressed)
    {
        this->translate_x = e->x - this->press_x + this->translate_x_pers;
        this->translate_y = e->y - this->press_y + this->translate_y_pers;
        this->queue_draw();
    }

    return true;
}

void CL::Map::store_visits()
{
    std::ofstream fon(this->visits_path + "visits.dat");

    for (CL::Country const &country : this->countries)
    {
        if (country.visited)
        {
            fon << country.name << "#" << country.visits << std::endl;
        }
    }
}

void CL::Map::load_map(const std::string &filename)
{

// check if directory exists
#ifdef WIN32
    this->visits_path = std::string(getenv("APPDATA")) + "\\CaptLog\\";
#else
    this->visits_path = Glib::get_home_dir() + "/.captlog/";
#endif

    struct stat sb;

    if (stat(this->visits_path.c_str(), &sb) != 0 || !S_ISDIR(sb.st_mode))
        MKDIR(this->visits_path.c_str());

    std::ifstream fin((this->visits_path + "visits.dat").c_str());

    std::vector<CL::Country> visits;

    if (fin.fail())
    {
        std::ofstream visits_file_tmp(this->visits_path + "visits.dat");
        visits_file_tmp << std::endl;
    }
    else
    {
        std::ifstream visit_file(this->visits_path + "visits.dat");

        std::string buffer, name_tmp;
        int visits_tmp;
        while (std::getline(visit_file, buffer))
        {
            //FORMAT: name#N
            size_t pos = 0;
            if ((pos = buffer.find('#')) != std::string::npos)
            {
                name_tmp = buffer.substr(0, pos);
                visits_tmp = std::stoi(buffer.substr(pos + 1, buffer.length() - pos));
                visits.push_back(CL::Country(name_tmp, true, visits_tmp));
            }
        }
    }

    std::ifstream mapfile(filename);

    std::string buffer;
    float coord_x, coord_y;

    while (std::getline(mapfile, buffer))
    {
        std::istringstream iss(buffer);

        if (iss >> coord_x >> coord_y)
        {
            /* add coordinates to the last country */
            this->countries.back().coordinates.back().push_back(Coordinate(coord_x, coord_y));

            /* store min and max values */
            if (coord_x > this->high_x)
                this->high_x = coord_x;

            if (coord_y > this->high_y)
                this->high_y = coord_y;

            if (coord_x < this->low_x)
                this->low_x = coord_x;

            if (coord_y < this->low_y)
                this->low_y = coord_y;
        }
        else if (buffer == "coordinates")
        {
            /* Add new coordinates vector */
            this->countries.back().coordinates.push_back(std::vector<Coordinate>());
        }
        else
        {
            /* new country, coordinates ended */
            this->countries.push_back(Country());
            this->countries.back().name = buffer;

            for (CL::Country &country : visits)
            {
                if (country.name == buffer)
                {
                    this->countries.back().visited = true;
                    this->countries.back().visits = country.visits;
                    break;
                }
            }
        }
    }
}

bool CL::Map::add_visit(const Glib::ustring &country_name)
{
    for (CL::Country &country : this->countries)
    {
        if (country.name == country_name)
        {
            country.increment_visits();
            return true;
        }
    }

    return false;
}

bool CL::Map::add_visit(const unsigned int country_id)
{
    if (country_id < this->countries.size())
    {
        this->countries[country_id].increment_visits();
        return true;
    }
    else
        return false;
}

void CL::Map::store_drawing_to(Glib::ustring file_name)
{
    this->store = true;
    this->store_filename = file_name;

    this->queue_draw();
}