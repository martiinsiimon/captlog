#pragma once

#include "country.h"

#include <string>
#include <vector>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated"
#include <gtkmm/drawingarea.h>
#pragma GCC diagnostic pop

#ifdef WIN32
#include <windows.h>
#define MKDIR(x) CreateDirectory(x, NULL)
#else
#define MKDIR(x) mkdir(x, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)
#endif

namespace CL
{

class Map : public Gtk::DrawingArea
{
public:
  Map();

  bool add_visit(const Glib::ustring &country_name);
  bool add_visit(const unsigned int country_id);
  void store_visits();

  void store_drawing_to(Glib::ustring file_name);

  std::vector<Country> countries;

protected:
  bool on_draw(const Cairo::RefPtr<Cairo::Context> &cr) override;
  bool on_scroll_event(GdkEventScroll *e) override;
  bool on_button_press_event(GdkEventButton *e) override;
  bool on_motion_notify_event(GdkEventMotion *e) override;

private:
  float scale;
  float translate_x;
  float translate_y;
  float translate_x_pers;
  float translate_y_pers;

  float press_x;
  float press_y;

  float low_x;
  float low_y;
  float high_x;
  float high_y;

  constexpr static float base_width = 1000.0f;
  constexpr static float base_height = 600.0f;

  constexpr static float min_scale = 0.5f;
  constexpr static float max_scale = 5.0f;

  bool button_pressed;

  bool store;
  Glib::ustring store_filename;

  void load_map(const std::string &filename);

  std::string visits_path;
};

} // namespace CL
