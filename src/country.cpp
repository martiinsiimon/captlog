#include "country.h"
#include <vector>

CL::Country::Country() : name("Undefined"), visited(false), visits(0)
{
};


CL::Country::Country(std::string _name, bool _visited, int _visit_nr)
{
    this->name = _name;
    this->visited = _visited;
    this->visits = _visit_nr;
}

CL::Country::~Country()
{

}

void CL::Country::increment_visits()
{
    this->visits++;
    this->visited = true;
}

void CL::Country::decrement_visits()
{
    this->visits--;

    if (this->visits < 0)
        this->visits = 0;

    if (this->visits == 0)
        this->visited = false;
}
