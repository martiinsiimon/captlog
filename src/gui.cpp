#include "map.h"
#include "window.h"

#include <gtkmm/application.h>

int main(int argc, char *argv[])
{
    auto app = Gtk::Application::create(argc, argv, "org.martiinsiimon.captlog");

    CL::Map map;

    CL::MainWindow window(map);

    return app->run(window);
}
