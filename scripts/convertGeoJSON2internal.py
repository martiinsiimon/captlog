#!/usr/bin/python

import json
import ipdb

JSON_FILENAME = "../data/usa.geojson"
RESULT_FILENAME = "result.dat"

if __name__ == "__main__":
    json_content = {}
    result = ""

    with open(JSON_FILENAME) as fil:
        json_content = json.load(fil)

    for state in json_content['features']:
        print("%s : %s" % (state['properties']
                           ['name'], state['geometry']['type']))

        # put a record for every state, starting with name
        result += "%s (USA)\n" % state['properties']['name']

        if state['geometry']['type'] == "Polygon":
            result += "coordinates\n"

            for coordinate_pair in state['geometry']['coordinates'][0]:
                result += "%f %f\n" % (coordinate_pair[0], coordinate_pair[1])

        elif state['geometry']['type'] == "MultiPolygon":
            # ipdb.set_trace()
            for coordinate_list in state['geometry']['coordinates']:
                result += "coordinates\n"

                for coordinate_pair in coordinate_list[0]:
                    result += "%f %f\n" % (coordinate_pair[0],
                                           coordinate_pair[1])
        else:
            print("ERR: Unrecognized type of coordinate list!")

    with open(RESULT_FILENAME, "w") as fil:
        fil.write(result)
    ipdb.set_trace()
