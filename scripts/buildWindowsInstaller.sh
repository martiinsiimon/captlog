#!/bin/bash

# This script is designed to build Windows installer

# go one step up
cd ..

# do a backup
cp captlog.nsi captlog.nsi.back

# sed the version
current_version=`cat version`
sed -i "s/XXXX_VERSION_XXXX/${current_version}/" captlog.nsi

# create package
makensis captlog.nsi

# move installer binary to releases directory
mv captlog-${current_version}-win64.exe releases/

# restore backup
mv captlog.nsi.back captlog.nsi

# go back to the scripts
cd scripts