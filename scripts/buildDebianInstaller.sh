#!/bin/bash

# This script is designed to build debian package

# create temporary directory
tempdir=`mktemp -d -t captlogXXXXX`

if [[ ! "${tempdir}" || ! -d "${tempdir}" ]]; then
  echo "Could not create temp dir"
  exit 1
fi

# store current dir
currdir=`pwd`

# get the version
current_version=`cat ${currdir}/../version`

# create installation structure
mkdir ${tempdir}/captlog_${current_version}
mkdir ${tempdir}/captlog_${current_version}/usr
mkdir ${tempdir}/captlog_${current_version}/usr/local
mkdir ${tempdir}/captlog_${current_version}/usr/local/bin
cp ${currdir}/../build_linux/src/captlog ${tempdir}/captlog_${current_version}/usr/local/bin/

mkdir ${tempdir}/captlog_${current_version}/usr/local/share
mkdir ${tempdir}/captlog_${current_version}/usr/local/share/captlog
mkdir ${tempdir}/captlog_${current_version}/usr/local/share/captlog/data
cp ${currdir}/../data/map.dat ${tempdir}/captlog_${current_version}/usr/local/share/captlog/data/
# TODO add other stuff, like icons

# create installation structure - Debian structure
mkdir ${tempdir}/captlog_${current_version}/DEBIAN
cp ${currdir}/../data/debian_control.template ${tempdir}/captlog_${current_version}/DEBIAN/control
sed -i "s/XXXX_VERSION_XXXX/${current_version}/" ${tempdir}/captlog_${current_version}/DEBIAN/control

# create package
dpkg-deb --build ${tempdir}/captlog_${current_version}

# move installer binary to releases directory
mv ${tempdir}/captlog_${current_version}.deb ${currdir}/../releases/captlog_${current_version}_amd64.deb

# remove temporary dir
rm -rf ${tempdir}

# go back to the scripts
cd ${currdir}